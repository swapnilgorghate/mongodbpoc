﻿using ConsoleApplication4.Model;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace ConsoleApplication4
{
	public class DML
	{
		//var credentials = MongoCredential.CreateMongoCRCredential("Datagames", "Datagames", "Datagames@12");
		const string HOST_IP = "52.237.236.157";
		const int HOST_PORT = 27017;
		MongoClient client;
		IMongoDatabase database;

		MongoClientSettings settings;

		public DML()
		{
			settings = new MongoClientSettings
			{
				Server = new MongoServerAddress(HOST_IP, HOST_PORT)
			};
			client = new MongoClient(settings);
			database = client.GetDatabase("Datagames");
		}

		public IEnumerable<Rule> GetRules()
		{
			return database.GetCollection<Rule>("Rule")?.AsQueryable();
		}

		public IEnumerable<RuleModel> GetRuleModels()
		{
			return database.GetCollection<RuleModel>("RuleModel")?.AsQueryable();
		}

		public IEnumerable<BsonDocument> GetDataJson()
		{
			return database.GetCollection<BsonDocument>("DataJson").AsQueryable();
		}

		public IMongoCollection<BsonDocument> GetDataJsonDocument()
		{
			return database.GetCollection<BsonDocument>("DataJson");
		}

		public void InsertRules()
		{
			var ruleModel = new RuleModel
			{
				ActivityImageId = 1,
				Aggregate = "Each Record",
				ActivityImageURL = string.Empty,
				APIDateFieldId = 25,
				APIDateFieldName = "Peerformance",
				APIFieldId = 50,
				APIFieldName = "Wrap",
				APIListId = 20,
				APIListName = "Kepler",
				CampaignId = 1,
				CampaignName = "datagames",
				CountNumber = 2,
				DateCreated = DateTime.Now,
				DateRange = "This Calendar Year",
				Description = "Test",
				Filters = new List<RuleFilter>
				{
					new RuleFilter
					{
						APIFieldId = 1,
						APIFieldName ="Test",
						Id =2,
						LogicalOperator ="<=",
						Sequence= 2,
						TenantId =29,
						Type = "test",
						Value = "100",
						APIListId=10,
						APIListName ="Kepler",
						Operator ="*"
					}
				},
				Operator = "<=",
				UserCreated = 1023,
				Value = "100",
				TenantId = 45,
				IsAgentBooster = false,
				IsBoosterActivity = false,
				IsHidden = false,
				IsOrFlag = false,
				Name = "Test activity",
				NextValue = "1000",
				Id = 10,
			};

			var document = database.GetCollection<RuleModel>("RuleModel");
			document.InsertOne(ruleModel);
		}

		public JToken ConvertBsonDocumentToJsonToken(BsonDocument document)
		{
			var jsonWriterSettings = new JsonWriterSettings { OutputMode = JsonOutputMode.Strict };
			var json = JValue.Parse(document.ToJson<BsonDocument>(jsonWriterSettings));
			return json;
		}

		public IEnumerable<JToken> ConvertBsonDocumentToJsonToken(IEnumerable<BsonDocument> documents)
		{
			var jsonWriterSettings = new JsonWriterSettings { OutputMode = JsonOutputMode.Strict };
			var jsons = new List<JToken>();
			foreach (var document in documents)
			{
				jsons.Add(JValue.Parse(document.ToJson<BsonDocument>(jsonWriterSettings)));
			}
			return jsons;

		}

		public dynamic GetJsonProperty(JToken token, string fieldName)
		{
			return token[fieldName];
		}

		public void JoinCollections()
		{
			var rules = database.GetCollection<Rule>("Rule")?.AsQueryable();

			// DataJson will be an dynamic object
			// Need to find a way to deserialize/create datajson class dynamically based on Metadata 
			var dataJsons = database.GetCollection<DataJson>("DataJson").AsQueryable();

			foreach (var item in rules.Where(r => r.Operator.Equals("=")))
			{
				var filteredDataJsons = dataJsons.ToList().Where(d =>
							d.AnnualRevenue == item.RuleOperationValue);

				foreach (var filteredDataJson in filteredDataJsons)
				{
					Console.WriteLine(filteredDataJson.AnnualRevenue);
				}
			}

			foreach (var item in rules.Where(r => r.Operator.Equals(">")))
			{
				var value = Convert.ToDecimal(item.RuleOperationValue);
				var filteredDataJsons = dataJsons.ToList().Where(d =>
							Convert.ToDecimal(d.AnnualRevenue) >= value);

				foreach (var filteredDataJson in filteredDataJsons)
				{
					Console.WriteLine(filteredDataJson.AnnualRevenue);
				}
			}

			//var filter = rules.Join(dataJsons,a => a.RuleOperationValue,
			//	b => b.AnnualRevenue,
			//	(a, b) => new { a.RuleOperationValue, b.AnnualRevenue });
			//var adsa = filter.ToList();

			Console.ReadLine();
		}
	}
}
