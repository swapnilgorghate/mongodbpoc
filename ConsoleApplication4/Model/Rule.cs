﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4.Model
{
	public class Rule
	{
		[BsonId]
		public ObjectId _id { get; set; }
		[BsonElement("Name")]
		public string Name { get; set; }
		[BsonElement("ApiId")]
		public string ApiId { get; set; }
		[BsonElement("ApiName")]
		public string ApiName { get; set; }
		[BsonElement("Aggregate")]
		public string Aggregate { get; set; }
		[BsonElement("FieldName")]
		public string FieldName { get; set; }
		[BsonElement("Operator")]
		public string Operator { get; set; }
		[BsonElement("RuleOperationValue")]
		public string RuleOperationValue { get; set; }
		[BsonElement("RuleDateFilterId")]
		public string RuleDateFilterId { get; set; }
		[BsonElement("DateRange")]
		public string DateRange { get; set; }
		[BsonElement("Points")]
		public int Points { get; set; }
	}
}