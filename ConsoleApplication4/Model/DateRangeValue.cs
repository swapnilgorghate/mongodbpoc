﻿using System;

namespace ConsoleApplication4.Model
{
	public class DateRangeValue
	{
		public DateTime FromDateTime { get; set; }
		public DateTime ToDateTime { get; set; }
	}
}