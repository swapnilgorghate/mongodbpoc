﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace ConsoleApplication4.Model
{
	public class RuleModel
	{
		[BsonId]
		public ObjectId _id { get; set; }
		[BsonElement("Id")]
		public int Id { get; set; }
		[BsonElement("Name")]
		public string Name { get; set; }
		[BsonElement("Description")]
		public string Description { get; set; }
		[BsonElement("CampaignId")]
		public long CampaignId { get; set; }
		[BsonElement("CampaignName")]
		public string CampaignName { get; set; }
		[BsonElement("RoleId")]
		public int RoleId { get; set; }
		[BsonElement("RoleName")]
		public string RoleName { get; set; }
		[BsonElement("APIListId")]
		public int APIListId { get; set; }
		[BsonElement("APIListName")]
		public string APIListName { get; set; }
		[BsonElement("APIFieldId")]
		public int APIFieldId { get; set; }
		[BsonElement("APIFieldName")]
		public string APIFieldName { get; set; }
		[BsonElement("Aggregate")]
		public string Aggregate { get; set; }
		[BsonElement("Operator")]
		public string Operator { get; set; }
		[BsonElement("Value")]
		public string Value { get; set; }
		[BsonElement("NextValue")]
		public string NextValue { get; set; }
		[BsonElement("APIDateFieldId")]
		public int APIDateFieldId { get; set; }
		[BsonElement("APIDateFieldName")]
		public string APIDateFieldName { get; set; }
		[BsonElement("DateRange")]
		public string DateRange { get; set; }
		[BsonElement("Filters")]
		public List<RuleFilter> Filters { get; set; }
		[BsonElement("Points")]
		public string Points { get; set; }
		[BsonElement("TenantId")]
		public long TenantId { get; set; }
		[BsonElement("UserCreated")]
		public long UserCreated { get; set; }
		[BsonElement("DateCreated")]
		public DateTime DateCreated { get; set; }
		[BsonElement("UserModified")]
		public long? UserModified { get; set; }
		[BsonElement("DateModified")]
		public DateTime? DateModified { get; set; }
		[BsonElement("ActivityImageId")]
		public long ActivityImageId { get; set; }
		[BsonElement("ActivityImageURL")]
		public string ActivityImageURL { get; set; }
		[BsonElement("CountNumber")]
		public int? CountNumber { get; set; }
		[BsonElement("IsAgentBooster")]
		public bool? IsAgentBooster { get; set; }
		[BsonElement("IsBoosterActivity")]
		public bool IsBoosterActivity { get; set; }
		[BsonElement("IsHidden")]
		public bool IsHidden { get; set; }
		[BsonElement("AggregateForOr")]
		public string AggregateForOr { get; set; }
		[BsonElement("OperatorForOr")]
		public string OperatorForOr { get; set; }
		[BsonElement("PointsForOr")]
		public string PointsForOr { get; set; }
		[BsonElement("ValueForOr")]
		public string ValueForOr { get; set; }
		[BsonElement("NextValueForOr")]
		public string NextValueForOr { get; set; }
		[BsonElement("IsOrFlag")]
		public bool IsOrFlag { get; set; }
		[BsonElement("CountNumberForOr")]
		public int? CountNumberForOr { get; set; }
		[BsonElement("RuleOperationID")]
		public int RuleOperationID { get; set; }
		[BsonElement("RuleOperationIDForOr")]
		public int RuleOperationIDForOr { get; set; }
		[BsonElement("APIFieldIdForOr")]
		public int APIFieldIdForOr { get; set; }
		[BsonElement("APIFieldNameForOr")]
		public string APIFieldNameForOr { get; set; }
	}
}