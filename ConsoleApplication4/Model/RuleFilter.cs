﻿using MongoDB.Bson.Serialization.Attributes;

namespace ConsoleApplication4.Model
{
	public class RuleFilter
	{
		[BsonElement("Id")]
		public int Id { get; set; }
		[BsonElement("APIListId")]
		public int APIListId { get; set; }
		[BsonElement("APIListName")]
		public string APIListName { get; set; }
		[BsonElement("APIFieldId")]
		public int APIFieldId { get; set; }
		[BsonElement("APIFieldName")]
		public string APIFieldName { get; set; }
		[BsonElement("Operator")]
		public string Operator { get; set; }
		[BsonElement("Value")]
		public string Value { get; set; }
		[BsonElement("Sequence")]
		public int Sequence { get; set; }
		[BsonElement("LogicalOperator")]
		public string LogicalOperator { get; set; }
		[BsonElement("TenantId")]
		public long TenantId { get; set; }
		[BsonElement("Type")]
		public string Type { get; set; }
	}
}
