﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4.Model
{
	public class DataJson
	{
		[BsonId]
		public ObjectId _id { get; set; }
		[BsonElement("AnnualRevenue")]
		public string AnnualRevenue { get; set; }
	}
}
