﻿using ConsoleApplication4.Utility;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Linq;

namespace ConsoleApplication4
{
	class Program
	{
		static void Main(string[] args)
		{
			var dml = new DML() { };
			//dml.JoinCollections();
			dml.GetDataJson();
			//dml.InsertRules();
			// need to create async method
			var data = dml.GetDataJson().FirstOrDefault();
			var ruleModels = dml.GetRuleModels();

			// need to create async method
			var allDocuments = dml.GetDataJsonDocument();
			var builder = Builders<BsonDocument>.Filter;
			// need to create async method
			var firstRule = ruleModels.FirstOrDefault(); //Just for testing

			//var filter = builder.Eq(firstRule.APIFieldName, double.Parse(firstRule.Value, CultureInfo.InvariantCulture));
			var jsonObject = dml.ConvertBsonDocumentToJsonToken(data);

			foreach (var ruleModel in ruleModels.Where(r => r.Aggregate.Equals("Each Record")))
			{
				Console.WriteLine($"Documents satisfying operator: {ruleModel.Operator}");
				var filter = Helper.GetDocumentFilter(ruleModel.Operator, ruleModel.APIFieldName, ruleModel.Value);
				var filterdocs = allDocuments.Find(filter);
				foreach (var item in filterdocs.ToList())
				{
					Console.WriteLine(item);
				}
			}
			Console.ReadLine();
		}
	}
}
	