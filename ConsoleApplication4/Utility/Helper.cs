﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Globalization;
using System;
using ConsoleApplication4.Model;

namespace ConsoleApplication4.Utility
{
	public static class Helper
	{
		public static FilterDefinition<BsonDocument> GetDocumentFilter(
			string ruleOperator,
			string fieldName,
			string fieldValue,
			string fieldDataType = "STRING")
		{
			var builder = Builders<BsonDocument>.Filter;
			switch (ruleOperator)
			{
				case "=":
					return builder.Eq(fieldName, double.Parse(fieldValue, CultureInfo.InvariantCulture));
				case ">":
					return builder.Gt(fieldName, double.Parse(fieldValue, CultureInfo.InvariantCulture));
				case "<":
					return builder.Lt(fieldName, double.Parse(fieldValue, CultureInfo.InvariantCulture));
				case "=<":
					return builder.Lte(fieldName, double.Parse(fieldValue, CultureInfo.InvariantCulture));
				case "=>":
					return builder.Gte(fieldName, double.Parse(fieldValue, CultureInfo.InvariantCulture));
				case "<>":
					return builder.Ne(fieldName, double.Parse(fieldValue, CultureInfo.InvariantCulture));
				default:
					return null;
			}
		}

		public static DateRangeValue GetDateTime(string dateRange)
		{
			switch (dateRange)
			{
				case "This (Calender) Year":
					int year = DateTime.Now.Year;
					return new DateRangeValue
					{
						FromDateTime = new DateTime(year, 1, 1),
						ToDateTime = new DateTime(year, 12, 31)
					};
				default:
					break;
			}
			return null;
		}
	}
}
